<?php
namespace models;

use ActiveRecord\Model;

class Pages extends Model {
    public static $table_name = 'pages';

    static $before_save = ['beforeSave'];


    public function beforeSave() {
        $this->hash = self::hashUrl($this->url);
    }

    /**
     * @param $url
     * @return bool
     */
    public static function hasPage($url) {
        return (bool)static::count(['hash' => static::hashUrl($url)]);
    }

    /**
     * @param $url
     * @return string
     */
    public static function hashUrl($url) {
        return md5($url);
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function getPage($url) {
        return static::find(['hash' => static::hashUrl($url)]);
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function getTree($url) {
        return static::find('all', ['conditions' => 'url LIKE "%' . $url . '%"']);
    }
}
