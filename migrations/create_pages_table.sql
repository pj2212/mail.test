DROP TABLE IF EXISTS pages;
CREATE TABLE `pages` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`hash` char(64) NOT NULL DEFAULT '',
`url` char(50) NOT NULL DEFAULT '',
`title` char(11) DEFAULT NULL,
`name` char(11) DEFAULT NULL,
`storage_path` char(100) NOT NULL DEFAULT '',
`parent` char(64) NOT NULL DEFAULT '',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8mb4;