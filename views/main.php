<?php
    if(isset($data)) {
        $title = $data['title'];
        $text = $data['text'];
        $tree = $data['tree'];
    }
?>
<html>
<head>
    <title>Просмотр страницы</title>
</head>
    <body>
        <h1><?=$title?></h1>
        <div><?=\modules\TreeHelper::formatText($text)?></div>
            <div>
                <?=\modules\TreeHelper::recTreeWalk($tree)?>
            </div>
    </body>
</html>
