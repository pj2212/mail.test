<?php

$connections = [
    'development' => 'mysql://root:root@127.0.0.1/wiki',
];

//@todo Обычно я не оставляю такие вещи в коде, но в этот раз можно
\ActiveRecord\Config::initialize(function ($cfg) {
    $cfg->set_model_directory(__ROOT__ . 'models');
    $cfg->set_connections($connections);
});