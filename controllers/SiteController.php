<?php
namespace controllers;

use controllers\base\BaseController;
use models\Files;
use models\Pages;
use modules\Request;
use modules\TreeHelper;

class SiteController extends BaseController {

    /**
     *
     */
    public function actionError() {
        $this->renderError();
        return;
    }

    /**
     * @param $path
     */
    public function actionView($path) {
        if(Pages::hasPage($path)) {
            $page = Pages::getPage($path);
            $tree = TreeHelper::parseTree($path);
            $data = [
                'title' => htmlspecialchars($page->title),
                'text' => file_get_contents('.'.$page->storage_path),
                'tree' => $tree
            ];
            $this->render('views/main.php', $data);
        } else {
            $this->redirect('/error');
        }
    }

    /**
     * @param $path
     */
    public function actionEdit($path) {
        $main_path = str_replace('/edit/', '', $path);
        $main_path = empty($main_path) ? '/': $main_path . '/';

        if(!Pages::hasPage($main_path)) {
            $this->redirect('/error');
        }
        $page = Pages::getPage($main_path);
        $data = [
            'name' => htmlspecialchars($page->name),
            'title' => htmlspecialchars($page->title),
            'content' => file_get_contents('.'.$page->storage_path),
        ];

        if (Request::isPost()) {

            //@todo Можно перенести логику проверки данных в модель
            $edited_data = Request::getPostData();
            $name =  $edited_data['name'];
            $url = str_replace($data['name'], $edited_data['name'], $main_path);
            $title = $edited_data['title'];
            $content = $edited_data['content'];

            $page->set_attributes([
                'url' => $url,
                'title' => $title,
                'name' => $name,
                'storage_path' => '/storage/' .$name . '.html'
            ]);

            if ($page->save()) {
                $file = new Files();
                $file->contentSave($page->storage_path, $content);
                $this->redirect($url);
                return;
            } else {
                $this->redirect('/error');
                return;
            }

        }

        $this->render('views/add.php', $data);
        return;
    }

    /**
     * @param $path
     */
    public function actionAdd($path) {
        $main_path = str_replace('/add/', '', $path);
        $main_path = empty($main_path) ? '/': $main_path . '/';

        if (Request::isPost()) {
            $new_page = new Pages();

            //@todo Можно перенести логику проверки данных в модель
            $data = Request::getPostData();
            $name =  $data['name'];
            $url = $main_path . $name . '/';
            $title = $data['title'];
            $content = $data['content'];

            $new_page->set_attributes([
                'url' => $url,
                'parent' => $main_path,
                'title' => $title,
                'name' => $name,
                'storage_path' => '/storage/' .$name . '.html'
            ]);

            if(Pages::hasPage($url)) {
                $this->redirect($url);
                return;
            }

            if ($new_page->save()) {
                $file = new Files();
                $file->contentSave($new_page->storage_path, $content);
                $this->redirect($url);
                return;
            } else {
                $this->redirect('/error');
                return;
            }

        }

        if(Pages::hasPage($main_path) || $main_path === '/') {
            $this->render('views/add.php');
        } else {
            $this->redirect('/error');
        }
    }
}