<?php
namespace controllers\base;

class BaseController {

    /**
     * @param $url
     */
    public function redirect($url) {
        header( 'Location: ' . $url, true, 303 );
    }

    /**
     * @param $path
     * @param array $data
     */
    public function render($path, $data = []) {
        require_once __ROOT__ . '/' . $path;
        exit();

    }

    /**
     *
     */
    public function renderError() {
        require_once __ROOT__ . 'views/error.html';
        exit();
    }
}