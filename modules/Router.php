<?php
namespace modules;

use controllers\SiteController;

//todo Конечно, это не полноценный роутер..
class Router {
    private $request = null;

    /**
     * @param $request
     * @return $this
     */
    public function setRequest($request) {
        $this->request = $request;
        return $this;
    }

    /**
     *
     */
    public function processUrl() {
        $url = parse_url($this->request);
        $path = $url['path'];
        //@ ..todo в настоящем роутере такого не будет
        $controller = (new SiteController());
        if(stripos ($path, 'add/', -4) !== false) {
            $controller->actionAdd($path);
        } elseif (stripos ($path, 'edit/', -5) !== false) {
            $controller->actionEdit($path);
        } elseif (stripos ($path, 'error/', -6) !== false) {
            $controller->actionError();
        } else {
            $controller->actionView($path);
        }

        return;
    }

    /**
     *
     */
    public function run() {
        $this->processUrl();
    }

    /**
     * @param $url
     */
    public function redirect($url) {
        header( 'Location: ' . $url, true, 303 );
    }
}