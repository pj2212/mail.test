<?php
namespace modules;

use models\Pages;

//@todo  вполне, можно было бы оформить все это компонентом/виджетом и
//@todo просто подключать в каком-нибудь представлении
class TreeHelper {
    /**
     * @param $path
     * @return array
     */
    public static function parseTree($path) {
        $data = Pages::getTree($path);
        $tree = [];
        foreach ($data as $key => $d) {
            if($key == 0) {
                continue;
            }
            $tree[$d->parent][] = $d->url;
        }
    return $tree;
    }

    public static function recTreeWalk($tree) {
        sort($tree);
        $res = array_reduce($tree, function ($last, $current) {
            $res = '';
            foreach ($current as $c) {
                $prev = $last;
                if(stripos($last, $c) === false) {
                    $prev = '';
                }
                $res .= '<li>' . $c . $prev . '</li>';
            }
            return '<ul>' . $res . '</ul>';
        }
            , '');
        if($res) {
            $res = '<h3>Дерево подстраниц</h3>' . $res;
        }
        return $res;
    }

    public static function formatText($text) {
        $text = preg_replace( '/\*\*([^\*]*)\*\*/', "<b>$1</b>", $text );
        $text = preg_replace( "/\\\\([^\\\\]+)\\\\/", "<i>$1</i>", $text );
        $text = preg_replace( '/\(\(([^\(|\)]*)\[([^\(|\)]*)\]\)\)/', "<a href='$1'>$2</a>", $text );
        return $text;
    }
}