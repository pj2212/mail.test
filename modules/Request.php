<?php
namespace modules;

class Request {
    /**
     * @return bool
     */
    public static function isPost() {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    /**
     * @return bool
     */
    public static function isGet() {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    /**
     * @return array
     */
    public static function getPostData() {
        $data = $_POST;
        $res = [];

        foreach ($data as $key => $val) {
            $res[$key] = htmlspecialchars($val);
        }

        return $res;
    }
}