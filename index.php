<?php
require_once 'vendor/autoload.php';
require_once './config/config.php';
require_once './config/active_record_init.php';

use modules\Router;

$request = null;
if (isset($_SERVER['REQUEST_URI'])) {
    $request = $_SERVER['REQUEST_URI'];
}

(new Router())->setRequest($request)->run();

function ea($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    die();
}